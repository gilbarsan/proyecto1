FROM debian:latest

MAINTAINER gilbarsan <gilbarsan@gmail.com>

RUN apt-get update

RUN apt-get install apache2 php-mysql php libapache2-mod-php  mysql-server wget vim curl  debconf-utils -y

# MySQL
ENV MYSQL_PWD root123
RUN echo "mysql-server mysql-server/root_password password $MYSQL_PWD" | debconf-set-selections
RUN echo "mysql-server mysql-server/root_password_again password $MYSQL_PWD" | debconf-set-selections
RUN apt-get -y install mysql-server



#services and libs installation.
RUN /etc/init.d/mysql start  && mysql -u root -proot -e "create database wordpress" && mysql -uroot -proot -e "grant  all on wordpress.* to 'wordpress'@'localhost' identified  by 'dbpassword' ; flush privileges"
RUN chown www-data:www-data /var/www/html/ -R


RUN cd /tmp;wget https://wordpress.org/latest.tar.gz
RUN cd /var/www/html; rm index.html ; tar xvzf /tmp/latest.tar.gz; mv wordpress/* . ; rm -rf wordpress

COPY wp-config.php /var/www/html/wp-config.php


#SSH
RUN apt-get install ssh -y
RUN mkdir /home/gilbarsan ; useradd gilbarsan -d /home/gilbarsan -s /bin/bash
RUN echo "gilbarsan:gilbarsan" | chpasswd gilbarsan
RUN mkdir /var/run/sshd

#Supervisor config
RUN apt-get install supervisor -y
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf


EXPOSE 22 80
CMD ["/usr/bin/supervisord"] 